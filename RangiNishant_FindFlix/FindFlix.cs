﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RangiNishant_FindFlix
{
    //using Xmlroot attribute to provide schema information for the class that is going ot be the root element of the object graph beibg serialized.
    [Serializable, XmlRoot(ElementName = "findFlix", IsNullable = true)]
    public class FindFlix
    {
        //Under the root element  - element named movies will be recognized while reading the xml and printed when xml is being created
        [XmlArrayItem(ElementName = "movie", Type = typeof(MovieData))]
        [XmlArray]
        
        //sibling nodes will contain properties of the objects in the BindingList that we have created named Watchlist fo rall the items in that list
        public BindingList<MovieData> Watchlist { get; set; }


        //Under the root element  - element named movies will be recognized while reading the xml and printed when xml is being created
        [XmlArrayItem(ElementName = "movie", Type = typeof(MovieData))]
        [XmlArray]

        //sibling nodes will contain properties of the objects in the BindingList that we have created named Favorites fo rall the items in that list
        public BindingList<MovieData> Favorites { get; set; }

    }
}
