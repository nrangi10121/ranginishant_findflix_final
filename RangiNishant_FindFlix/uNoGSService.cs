﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace RangiNishant_FindFlix
{
    public class uNoGSService
    {
        //API added in the class uNoGSService. findMovie takes in the search request by user to find that in the API by getAPIClient returning a list of MovieData.
        public List<MovieData> findMovie(string title, out  string errorMessages) 
        {
            var client = getApiClient($"{OmdbUrl}?s={title}");
            var request = new RestRequest(Method.GET);
            var responce = client.Execute(request);
            errorMessages = string.Empty;

            JObject searchResultObj = JObject.Parse(responce.Content);

            string results0 = (string)searchResultObj["Response"];

            if(results0 == "False")
            {
                errorMessages = (string)searchResultObj["Error"];
                return null;
            }



            
            // get JSON result objects into a list
            IList<JToken> results = searchResultObj["Search"].Children().ToList();

            // serialize JSON results into .NET objects
            List<MovieData> searchResults = new List<MovieData>();
            foreach (JToken result in results)
            {
                // JToken.ToObject is a helper method that uses JsonSerializer internally
                MovieData searchResult = result.ToObject<MovieData>();
                searchResults.Add(searchResult);
            }


            return searchResults;

        }

        //getMovie Method takes in imdbID to get all the information of the movie because simple search by title was not returning all the needed information. 
        public MovieData getMovie(string imdbID)
        {
            var client = getApiClient($"{OmdbUrl}?i={imdbID}");
            var request = new RestRequest(Method.GET);
            var responce = client.Execute(request);

            return JsonConvert.DeserializeObject<MovieData>(responce.Content);
        }

        private RestClient getApiClient(string URL) {

            if (URL.IndexOf("?") >= 0)
            {
                URL += "&apikey=b927c92f";
            }
            else
            {
                URL += "?apikey=b927c92f";
            }

            return new RestClient(URL);
        }
        private string OmdbUrl { get { return "http://www.omdbapi.com/"; } }

    }


}
