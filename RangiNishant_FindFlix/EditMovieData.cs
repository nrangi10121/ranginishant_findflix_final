﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RangiNishant_FindFlix
{
    //declaring a delegate
    public delegate void EventDelegate(object sender, MovieDataEventArgs e);
    public partial class EditMovieData : Form
    {

        //Property to save selected movie object
        public MovieData MovieToEdit { get; set; }

        //Declare Event Delegate for notify for Movie Updatess
        public event EventDelegate MovieDataEdited;

        public EditMovieData()
        {
            InitializeComponent();
        }

        private void buttonSaveMovieData_Click(object sender, EventArgs e)
        {
            MovieToEdit.movieNotes = textBoxNotes.Text;
            //calling event handler
            MovieDataEventArgs mD = new MovieDataEventArgs(MovieToEdit);
            

            MovieDataEdited(this, mD);
        }

        private void EditMovieData_Load(object sender, EventArgs e)
        {           
            textBoxTitle.Text = MovieToEdit.title;
            textBoxGenre.Text = MovieToEdit.genre;
            textBoxReleaseDate.Text = MovieToEdit.releaseDate.ToString();
            textBoxRated.Text = MovieToEdit.rated;
            textBoxIMDBRating.Text = MovieToEdit.imdbRating.ToString();
            textBoxDirector.Text = MovieToEdit.director;
            textBoxWriter.Text = MovieToEdit.writer;
            textBoxPlot.Text = MovieToEdit.plot;
            textBoxNotes.Text = MovieToEdit.movieNotes;

        } 
    }
}
