﻿namespace RangiNishant_FindFlix
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControlHome = new System.Windows.Forms.TabControl();
            this.tabPageHome = new System.Windows.Forms.TabPage();
            this.buttonDeleteFromFavorites = new System.Windows.Forms.Button();
            this.labelChangeColor = new System.Windows.Forms.Label();
            this.buttonMoveToWatchlist = new System.Windows.Forms.Button();
            this.radioButtonRed = new System.Windows.Forms.RadioButton();
            this.radioButtonOrange = new System.Windows.Forms.RadioButton();
            this.buttonDeleteFromWatchlist = new System.Windows.Forms.Button();
            this.buttonMoveToFavorites = new System.Windows.Forms.Button();
            this.listBoxFavorites = new System.Windows.Forms.ListBox();
            this.listBoxWatchlist = new System.Windows.Forms.ListBox();
            this.buttonAddToFavorites = new System.Windows.Forms.Button();
            this.buttonAddToWatchlist = new System.Windows.Forms.Button();
            this.listBoxMovieList = new System.Windows.Forms.ListBox();
            this.buttonSearchMovie = new System.Windows.Forms.Button();
            this.textBoxSearchMovie = new System.Windows.Forms.TextBox();
            this.labelMovieName = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelFindFlix = new System.Windows.Forms.Label();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.groupBox1.SuspendLayout();
            this.tabControlHome.SuspendLayout();
            this.tabPageHome.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControlHome);
            this.groupBox1.Controls.Add(this.menuStrip1);
            this.groupBox1.Location = new System.Drawing.Point(57, 282);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(674, 1074);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FindFlix";
            // 
            // tabControlHome
            // 
            this.tabControlHome.Controls.Add(this.tabPageHome);
            this.tabControlHome.Location = new System.Drawing.Point(6, 70);
            this.tabControlHome.Name = "tabControlHome";
            this.tabControlHome.SelectedIndex = 0;
            this.tabControlHome.Size = new System.Drawing.Size(665, 998);
            this.tabControlHome.TabIndex = 1;
            // 
            // tabPageHome
            // 
            this.tabPageHome.Controls.Add(this.buttonDeleteFromFavorites);
            this.tabPageHome.Controls.Add(this.labelChangeColor);
            this.tabPageHome.Controls.Add(this.buttonMoveToWatchlist);
            this.tabPageHome.Controls.Add(this.radioButtonRed);
            this.tabPageHome.Controls.Add(this.radioButtonOrange);
            this.tabPageHome.Controls.Add(this.buttonDeleteFromWatchlist);
            this.tabPageHome.Controls.Add(this.buttonMoveToFavorites);
            this.tabPageHome.Controls.Add(this.listBoxFavorites);
            this.tabPageHome.Controls.Add(this.listBoxWatchlist);
            this.tabPageHome.Controls.Add(this.buttonAddToFavorites);
            this.tabPageHome.Controls.Add(this.buttonAddToWatchlist);
            this.tabPageHome.Controls.Add(this.listBoxMovieList);
            this.tabPageHome.Controls.Add(this.buttonSearchMovie);
            this.tabPageHome.Controls.Add(this.textBoxSearchMovie);
            this.tabPageHome.Controls.Add(this.labelMovieName);
            this.tabPageHome.Location = new System.Drawing.Point(8, 39);
            this.tabPageHome.Name = "tabPageHome";
            this.tabPageHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHome.Size = new System.Drawing.Size(649, 951);
            this.tabPageHome.TabIndex = 0;
            this.tabPageHome.Text = "Home";
            this.tabPageHome.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteFromFavorites
            // 
            this.buttonDeleteFromFavorites.Enabled = false;
            this.buttonDeleteFromFavorites.Location = new System.Drawing.Point(337, 787);
            this.buttonDeleteFromFavorites.Name = "buttonDeleteFromFavorites";
            this.buttonDeleteFromFavorites.Size = new System.Drawing.Size(274, 49);
            this.buttonDeleteFromFavorites.TabIndex = 25;
            this.buttonDeleteFromFavorites.Text = "Delete From Favorites";
            this.buttonDeleteFromFavorites.UseVisualStyleBackColor = true;
            this.buttonDeleteFromFavorites.Click += new System.EventHandler(this.buttonDeleteFromFavorites_Click);
            // 
            // labelChangeColor
            // 
            this.labelChangeColor.AutoSize = true;
            this.labelChangeColor.Location = new System.Drawing.Point(48, 859);
            this.labelChangeColor.Name = "labelChangeColor";
            this.labelChangeColor.Size = new System.Drawing.Size(282, 25);
            this.labelChangeColor.TabIndex = 24;
            this.labelChangeColor.Text = "Change color of the controls";
            // 
            // buttonMoveToWatchlist
            // 
            this.buttonMoveToWatchlist.Enabled = false;
            this.buttonMoveToWatchlist.Location = new System.Drawing.Point(335, 732);
            this.buttonMoveToWatchlist.Name = "buttonMoveToWatchlist";
            this.buttonMoveToWatchlist.Size = new System.Drawing.Size(276, 49);
            this.buttonMoveToWatchlist.TabIndex = 23;
            this.buttonMoveToWatchlist.Text = "Move To Watchlist";
            this.buttonMoveToWatchlist.UseVisualStyleBackColor = true;
            this.buttonMoveToWatchlist.Click += new System.EventHandler(this.buttonMoveToWatchlist_Click);
            // 
            // radioButtonRed
            // 
            this.radioButtonRed.AutoSize = true;
            this.radioButtonRed.Location = new System.Drawing.Point(479, 857);
            this.radioButtonRed.Name = "radioButtonRed";
            this.radioButtonRed.Size = new System.Drawing.Size(86, 29);
            this.radioButtonRed.TabIndex = 22;
            this.radioButtonRed.TabStop = true;
            this.radioButtonRed.Text = "Blue";
            this.radioButtonRed.UseVisualStyleBackColor = true;
            this.radioButtonRed.CheckedChanged += new System.EventHandler(this.radioButtonRed_CheckedChanged);
            // 
            // radioButtonOrange
            // 
            this.radioButtonOrange.AutoSize = true;
            this.radioButtonOrange.Location = new System.Drawing.Point(359, 857);
            this.radioButtonOrange.Name = "radioButtonOrange";
            this.radioButtonOrange.Size = new System.Drawing.Size(82, 29);
            this.radioButtonOrange.TabIndex = 21;
            this.radioButtonOrange.TabStop = true;
            this.radioButtonOrange.Text = "Red";
            this.radioButtonOrange.UseVisualStyleBackColor = true;
            this.radioButtonOrange.CheckedChanged += new System.EventHandler(this.radioButtonOrange_CheckedChanged);
            // 
            // buttonDeleteFromWatchlist
            // 
            this.buttonDeleteFromWatchlist.Enabled = false;
            this.buttonDeleteFromWatchlist.Location = new System.Drawing.Point(35, 787);
            this.buttonDeleteFromWatchlist.Name = "buttonDeleteFromWatchlist";
            this.buttonDeleteFromWatchlist.Size = new System.Drawing.Size(274, 49);
            this.buttonDeleteFromWatchlist.TabIndex = 20;
            this.buttonDeleteFromWatchlist.Text = "Delete From Watchlist";
            this.buttonDeleteFromWatchlist.UseVisualStyleBackColor = true;
            this.buttonDeleteFromWatchlist.Click += new System.EventHandler(this.buttonDeleteFromWatchlist_Click);
            // 
            // buttonMoveToFavorites
            // 
            this.buttonMoveToFavorites.Enabled = false;
            this.buttonMoveToFavorites.Location = new System.Drawing.Point(35, 732);
            this.buttonMoveToFavorites.Name = "buttonMoveToFavorites";
            this.buttonMoveToFavorites.Size = new System.Drawing.Size(274, 49);
            this.buttonMoveToFavorites.TabIndex = 11;
            this.buttonMoveToFavorites.Text = "Move To Favorites";
            this.buttonMoveToFavorites.UseVisualStyleBackColor = true;
            this.buttonMoveToFavorites.Click += new System.EventHandler(this.buttonMoveToFavorites_Click);
            // 
            // listBoxFavorites
            // 
            this.listBoxFavorites.FormattingEnabled = true;
            this.listBoxFavorites.ItemHeight = 25;
            this.listBoxFavorites.Location = new System.Drawing.Point(335, 406);
            this.listBoxFavorites.Name = "listBoxFavorites";
            this.listBoxFavorites.Size = new System.Drawing.Size(276, 304);
            this.listBoxFavorites.TabIndex = 19;
            this.listBoxFavorites.SelectedIndexChanged += new System.EventHandler(this.listBoxFavorites_SelectedIndexChanged);
            this.listBoxFavorites.DoubleClick += new System.EventHandler(this.listBoxFavorites_DoubleClick);
            // 
            // listBoxWatchlist
            // 
            this.listBoxWatchlist.FormattingEnabled = true;
            this.listBoxWatchlist.ItemHeight = 25;
            this.listBoxWatchlist.Location = new System.Drawing.Point(35, 406);
            this.listBoxWatchlist.Name = "listBoxWatchlist";
            this.listBoxWatchlist.Size = new System.Drawing.Size(274, 304);
            this.listBoxWatchlist.TabIndex = 18;
            this.listBoxWatchlist.SelectedIndexChanged += new System.EventHandler(this.listBoxWatchlist_SelectedIndexChanged);
            this.listBoxWatchlist.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxWatchlist_MouseDoubleClick);
            // 
            // buttonAddToFavorites
            // 
            this.buttonAddToFavorites.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAddToFavorites.Location = new System.Drawing.Point(335, 353);
            this.buttonAddToFavorites.Name = "buttonAddToFavorites";
            this.buttonAddToFavorites.Size = new System.Drawing.Size(276, 47);
            this.buttonAddToFavorites.TabIndex = 17;
            this.buttonAddToFavorites.Text = "Add To Favorites";
            this.buttonAddToFavorites.UseVisualStyleBackColor = true;
            this.buttonAddToFavorites.Click += new System.EventHandler(this.buttonAddToFavorites_Click);
            // 
            // buttonAddToWatchlist
            // 
            this.buttonAddToWatchlist.Location = new System.Drawing.Point(35, 353);
            this.buttonAddToWatchlist.Name = "buttonAddToWatchlist";
            this.buttonAddToWatchlist.Size = new System.Drawing.Size(274, 47);
            this.buttonAddToWatchlist.TabIndex = 16;
            this.buttonAddToWatchlist.Text = "Add To Watchlist";
            this.buttonAddToWatchlist.UseVisualStyleBackColor = true;
            this.buttonAddToWatchlist.Click += new System.EventHandler(this.buttonAddToWatchlist_Click);
            // 
            // listBoxMovieList
            // 
            this.listBoxMovieList.FormattingEnabled = true;
            this.listBoxMovieList.ItemHeight = 25;
            this.listBoxMovieList.Location = new System.Drawing.Point(37, 104);
            this.listBoxMovieList.Name = "listBoxMovieList";
            this.listBoxMovieList.Size = new System.Drawing.Size(576, 229);
            this.listBoxMovieList.TabIndex = 15;
            // 
            // buttonSearchMovie
            // 
            this.buttonSearchMovie.Location = new System.Drawing.Point(506, 47);
            this.buttonSearchMovie.Name = "buttonSearchMovie";
            this.buttonSearchMovie.Size = new System.Drawing.Size(105, 39);
            this.buttonSearchMovie.TabIndex = 14;
            this.buttonSearchMovie.Text = "Search";
            this.buttonSearchMovie.UseVisualStyleBackColor = true;
            this.buttonSearchMovie.Click += new System.EventHandler(this.buttonSearchMovie_Click);
            // 
            // textBoxSearchMovie
            // 
            this.textBoxSearchMovie.Location = new System.Drawing.Point(185, 51);
            this.textBoxSearchMovie.Name = "textBoxSearchMovie";
            this.textBoxSearchMovie.Size = new System.Drawing.Size(299, 31);
            this.textBoxSearchMovie.TabIndex = 13;
            // 
            // labelMovieName
            // 
            this.labelMovieName.AutoSize = true;
            this.labelMovieName.BackColor = System.Drawing.Color.Transparent;
            this.labelMovieName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMovieName.Location = new System.Drawing.Point(30, 54);
            this.labelMovieName.Name = "labelMovieName";
            this.labelMovieName.Size = new System.Drawing.Size(142, 25);
            this.labelMovieName.TabIndex = 12;
            this.labelMovieName.Text = "Movie Name";
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 27);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(668, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(72, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(280, 44);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(280, 44);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(280, 44);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(280, 44);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // labelFindFlix
            // 
            this.labelFindFlix.AutoSize = true;
            this.labelFindFlix.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindFlix.Location = new System.Drawing.Point(312, 217);
            this.labelFindFlix.Name = "labelFindFlix";
            this.labelFindFlix.Size = new System.Drawing.Size(157, 42);
            this.labelFindFlix.TabIndex = 1;
            this.labelFindFlix.Text = "FindFlix";
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(781, 1521);
            this.BackgroundImage = global::RangiNishant_FindFlix.Properties.Resources.iPhone7Image1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(827, 1334);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelFindFlix);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControlHome.ResumeLayout(false);
            this.tabPageHome.ResumeLayout(false);
            this.tabPageHome.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label labelFindFlix;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Diagnostics.EventLog eventLog1;
        private System.Windows.Forms.TabControl tabControlHome;
        private System.Windows.Forms.TabPage tabPageHome;
        private System.Windows.Forms.Label labelChangeColor;
        private System.Windows.Forms.Button buttonMoveToWatchlist;
        private System.Windows.Forms.RadioButton radioButtonRed;
        private System.Windows.Forms.RadioButton radioButtonOrange;
        private System.Windows.Forms.Button buttonDeleteFromWatchlist;
        private System.Windows.Forms.Button buttonMoveToFavorites;
        private System.Windows.Forms.ListBox listBoxFavorites;
        private System.Windows.Forms.ListBox listBoxWatchlist;
        private System.Windows.Forms.Button buttonAddToFavorites;
        private System.Windows.Forms.Button buttonAddToWatchlist;
        private System.Windows.Forms.ListBox listBoxMovieList;
        private System.Windows.Forms.Button buttonSearchMovie;
        private System.Windows.Forms.TextBox textBoxSearchMovie;
        private System.Windows.Forms.Label labelMovieName;
        private System.Windows.Forms.Button buttonDeleteFromFavorites;
    }
}

