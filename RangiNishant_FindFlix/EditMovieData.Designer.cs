﻿namespace RangiNishant_FindFlix
{
    partial class EditMovieData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelGenre = new System.Windows.Forms.Label();
            this.labelReleaseDate = new System.Windows.Forms.Label();
            this.labelRated = new System.Windows.Forms.Label();
            this.labelImdbRating = new System.Windows.Forms.Label();
            this.labelDirector = new System.Windows.Forms.Label();
            this.labelWriter = new System.Windows.Forms.Label();
            this.labelPlot = new System.Windows.Forms.Label();
            this.labelNotes = new System.Windows.Forms.Label();
            this.buttonSaveMovieData = new System.Windows.Forms.Button();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxGenre = new System.Windows.Forms.TextBox();
            this.textBoxReleaseDate = new System.Windows.Forms.TextBox();
            this.textBoxRated = new System.Windows.Forms.TextBox();
            this.textBoxIMDBRating = new System.Windows.Forms.TextBox();
            this.textBoxDirector = new System.Windows.Forms.TextBox();
            this.textBoxWriter = new System.Windows.Forms.TextBox();
            this.textBoxPlot = new System.Windows.Forms.TextBox();
            this.textBoxNotes = new System.Windows.Forms.TextBox();
            this.labelFindFlix2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(30, 97);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(58, 25);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Title";
            // 
            // labelGenre
            // 
            this.labelGenre.AutoSize = true;
            this.labelGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGenre.Location = new System.Drawing.Point(30, 165);
            this.labelGenre.Name = "labelGenre";
            this.labelGenre.Size = new System.Drawing.Size(76, 25);
            this.labelGenre.TabIndex = 1;
            this.labelGenre.Text = "Genre";
            // 
            // labelReleaseDate
            // 
            this.labelReleaseDate.AutoSize = true;
            this.labelReleaseDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReleaseDate.Location = new System.Drawing.Point(30, 229);
            this.labelReleaseDate.Name = "labelReleaseDate";
            this.labelReleaseDate.Size = new System.Drawing.Size(154, 25);
            this.labelReleaseDate.TabIndex = 2;
            this.labelReleaseDate.Text = "Release Date";
            // 
            // labelRated
            // 
            this.labelRated.AutoSize = true;
            this.labelRated.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRated.Location = new System.Drawing.Point(32, 291);
            this.labelRated.Name = "labelRated";
            this.labelRated.Size = new System.Drawing.Size(74, 25);
            this.labelRated.TabIndex = 3;
            this.labelRated.Text = "Rated";
            // 
            // labelImdbRating
            // 
            this.labelImdbRating.AutoSize = true;
            this.labelImdbRating.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImdbRating.Location = new System.Drawing.Point(30, 352);
            this.labelImdbRating.Name = "labelImdbRating";
            this.labelImdbRating.Size = new System.Drawing.Size(143, 25);
            this.labelImdbRating.TabIndex = 4;
            this.labelImdbRating.Text = "IMDB Rating";
            // 
            // labelDirector
            // 
            this.labelDirector.AutoSize = true;
            this.labelDirector.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDirector.Location = new System.Drawing.Point(30, 423);
            this.labelDirector.Name = "labelDirector";
            this.labelDirector.Size = new System.Drawing.Size(95, 25);
            this.labelDirector.TabIndex = 5;
            this.labelDirector.Text = "Director";
            // 
            // labelWriter
            // 
            this.labelWriter.AutoSize = true;
            this.labelWriter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWriter.Location = new System.Drawing.Point(29, 492);
            this.labelWriter.Name = "labelWriter";
            this.labelWriter.Size = new System.Drawing.Size(75, 25);
            this.labelWriter.TabIndex = 6;
            this.labelWriter.Text = "Writer";
            // 
            // labelPlot
            // 
            this.labelPlot.AutoSize = true;
            this.labelPlot.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlot.Location = new System.Drawing.Point(32, 567);
            this.labelPlot.Name = "labelPlot";
            this.labelPlot.Size = new System.Drawing.Size(53, 25);
            this.labelPlot.TabIndex = 7;
            this.labelPlot.Text = "Plot";
            // 
            // labelNotes
            // 
            this.labelNotes.AutoSize = true;
            this.labelNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNotes.Location = new System.Drawing.Point(30, 755);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(73, 25);
            this.labelNotes.TabIndex = 8;
            this.labelNotes.Text = "Notes";
            // 
            // buttonSaveMovieData
            // 
            this.buttonSaveMovieData.Location = new System.Drawing.Point(458, 899);
            this.buttonSaveMovieData.Name = "buttonSaveMovieData";
            this.buttonSaveMovieData.Size = new System.Drawing.Size(111, 45);
            this.buttonSaveMovieData.TabIndex = 9;
            this.buttonSaveMovieData.Text = "Save";
            this.buttonSaveMovieData.UseVisualStyleBackColor = true;
            this.buttonSaveMovieData.Click += new System.EventHandler(this.buttonSaveMovieData_Click);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(239, 94);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.ReadOnly = true;
            this.textBoxTitle.Size = new System.Drawing.Size(330, 31);
            this.textBoxTitle.TabIndex = 10;
            // 
            // textBoxGenre
            // 
            this.textBoxGenre.Location = new System.Drawing.Point(239, 162);
            this.textBoxGenre.Name = "textBoxGenre";
            this.textBoxGenre.ReadOnly = true;
            this.textBoxGenre.Size = new System.Drawing.Size(330, 31);
            this.textBoxGenre.TabIndex = 11;
            // 
            // textBoxReleaseDate
            // 
            this.textBoxReleaseDate.Location = new System.Drawing.Point(239, 226);
            this.textBoxReleaseDate.Name = "textBoxReleaseDate";
            this.textBoxReleaseDate.ReadOnly = true;
            this.textBoxReleaseDate.Size = new System.Drawing.Size(330, 31);
            this.textBoxReleaseDate.TabIndex = 12;
            // 
            // textBoxRated
            // 
            this.textBoxRated.Location = new System.Drawing.Point(239, 288);
            this.textBoxRated.Name = "textBoxRated";
            this.textBoxRated.ReadOnly = true;
            this.textBoxRated.Size = new System.Drawing.Size(330, 31);
            this.textBoxRated.TabIndex = 13;
            // 
            // textBoxIMDBRating
            // 
            this.textBoxIMDBRating.Location = new System.Drawing.Point(239, 349);
            this.textBoxIMDBRating.Name = "textBoxIMDBRating";
            this.textBoxIMDBRating.ReadOnly = true;
            this.textBoxIMDBRating.Size = new System.Drawing.Size(330, 31);
            this.textBoxIMDBRating.TabIndex = 14;
            // 
            // textBoxDirector
            // 
            this.textBoxDirector.Location = new System.Drawing.Point(239, 420);
            this.textBoxDirector.Name = "textBoxDirector";
            this.textBoxDirector.ReadOnly = true;
            this.textBoxDirector.Size = new System.Drawing.Size(330, 31);
            this.textBoxDirector.TabIndex = 15;
            // 
            // textBoxWriter
            // 
            this.textBoxWriter.Location = new System.Drawing.Point(239, 489);
            this.textBoxWriter.Name = "textBoxWriter";
            this.textBoxWriter.ReadOnly = true;
            this.textBoxWriter.Size = new System.Drawing.Size(330, 31);
            this.textBoxWriter.TabIndex = 16;
            // 
            // textBoxPlot
            // 
            this.textBoxPlot.Location = new System.Drawing.Point(239, 564);
            this.textBoxPlot.MaximumSize = new System.Drawing.Size(330, 150);
            this.textBoxPlot.MinimumSize = new System.Drawing.Size(330, 150);
            this.textBoxPlot.Multiline = true;
            this.textBoxPlot.Name = "textBoxPlot";
            this.textBoxPlot.ReadOnly = true;
            this.textBoxPlot.Size = new System.Drawing.Size(330, 150);
            this.textBoxPlot.TabIndex = 19;
            // 
            // textBoxNotes
            // 
            this.textBoxNotes.Location = new System.Drawing.Point(239, 752);
            this.textBoxNotes.MaximumSize = new System.Drawing.Size(330, 100);
            this.textBoxNotes.MinimumSize = new System.Drawing.Size(330, 100);
            this.textBoxNotes.Multiline = true;
            this.textBoxNotes.Name = "textBoxNotes";
            this.textBoxNotes.Size = new System.Drawing.Size(330, 100);
            this.textBoxNotes.TabIndex = 20;
            // 
            // labelFindFlix2
            // 
            this.labelFindFlix2.AutoSize = true;
            this.labelFindFlix2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindFlix2.Location = new System.Drawing.Point(94, 9);
            this.labelFindFlix2.Name = "labelFindFlix2";
            this.labelFindFlix2.Size = new System.Drawing.Size(441, 42);
            this.labelFindFlix2.TabIndex = 21;
            this.labelFindFlix2.Text = "Add Note To This Movie";
            // 
            // EditMovieData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 974);
            this.Controls.Add(this.labelFindFlix2);
            this.Controls.Add(this.textBoxNotes);
            this.Controls.Add(this.textBoxPlot);
            this.Controls.Add(this.textBoxWriter);
            this.Controls.Add(this.textBoxDirector);
            this.Controls.Add(this.textBoxIMDBRating);
            this.Controls.Add(this.textBoxRated);
            this.Controls.Add(this.textBoxReleaseDate);
            this.Controls.Add(this.textBoxGenre);
            this.Controls.Add(this.textBoxTitle);
            this.Controls.Add(this.buttonSaveMovieData);
            this.Controls.Add(this.labelNotes);
            this.Controls.Add(this.labelPlot);
            this.Controls.Add(this.labelWriter);
            this.Controls.Add(this.labelDirector);
            this.Controls.Add(this.labelImdbRating);
            this.Controls.Add(this.labelRated);
            this.Controls.Add(this.labelReleaseDate);
            this.Controls.Add(this.labelGenre);
            this.Controls.Add(this.labelTitle);
            this.Name = "EditMovieData";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.EditMovieData_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelGenre;
        private System.Windows.Forms.Label labelReleaseDate;
        private System.Windows.Forms.Label labelRated;
        private System.Windows.Forms.Label labelImdbRating;
        private System.Windows.Forms.Label labelDirector;
        private System.Windows.Forms.Label labelWriter;
        private System.Windows.Forms.Label labelPlot;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.Button buttonSaveMovieData;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxGenre;
        private System.Windows.Forms.TextBox textBoxReleaseDate;
        private System.Windows.Forms.TextBox textBoxRated;
        private System.Windows.Forms.TextBox textBoxIMDBRating;
        private System.Windows.Forms.TextBox textBoxDirector;
        private System.Windows.Forms.TextBox textBoxWriter;
        private System.Windows.Forms.TextBox textBoxPlot;
        private System.Windows.Forms.TextBox textBoxNotes;
        private System.Windows.Forms.Label labelFindFlix2;
    }
}