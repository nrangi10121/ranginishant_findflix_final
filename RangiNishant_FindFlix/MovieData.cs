﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RangiNishant_FindFlix
{

    public class MovieData
    {
        [JsonProperty(PropertyName = "imdbID")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "Genre")]
        public string genre { get; set; }     
        
        [JsonProperty(PropertyName = "Year")]
        public int releaseDate { get; set; }

        [JsonProperty(PropertyName = "Type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "Ratings")]
        public List<Rating> PublicRatings { get; set; }

        [JsonProperty(PropertyName = "rated")]
        public string rated { get; set; }

        [JsonProperty(PropertyName = "imdbRating")]
        public decimal imdbRating { get; set; }

        [JsonProperty(PropertyName = "Director")]
        public string director { get; set; }

        [JsonProperty(PropertyName = "Writer")]
        public string writer { get; set; }
        //public List<string> actors = new List<string>() { get; set; }

        [JsonProperty(PropertyName = "Plot")]
        public string plot { get; set; }
        public string movieNotes { get; set; }


        public override string ToString()
        {
            return title + ", " + genre + ", " + rated + ", " + imdbRating + ", " + director + ", " + writer;
        }

        public string ToStringMessage()
        {
            return title + ", " + genre + ", " + rated + ", " + imdbRating + ", " + director + ", " + writer + ", "+ plot;
        }

    }

    public class Rating
    {
        public string Source { get; set; }
        public string Value { get; set; }
    }
}
