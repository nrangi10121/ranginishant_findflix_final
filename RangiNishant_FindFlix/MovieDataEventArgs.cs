﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RangiNishant_FindFlix
{
    public class MovieDataEventArgs : EventArgs
    {
        //instence of object MovieData is created
        MovieData m;

        public MovieData M { get => m; set => m = value; }
        public MovieDataEventArgs(MovieData m)
        {
            this.m = m;
        }

    }


}
