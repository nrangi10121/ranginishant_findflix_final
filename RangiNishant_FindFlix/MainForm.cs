﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace RangiNishant_FindFlix
{



    public partial class MainForm : Form
    {

        
        XmlSerializer serializer = new XmlSerializer(typeof(FindFlix));

        //List that will hold all the movie data
        BindingList<MovieData> _movieList = new BindingList<MovieData>();

        //List for Favorite and Watchlist items
        BindingList<MovieData> _movieListWatchlist = new BindingList<MovieData>();
        BindingList<MovieData> _movieListFavorites = new BindingList<MovieData>();

        FavoriteRepository favoriteRepository = new FavoriteRepository();
        WatchlistRepository watchlistRepository = new WatchlistRepository();

        public MovieData MovieToEdit { get; set; }


        public MainForm()
        {
            InitializeComponent();
            HandleClientWindowSize();
        }

        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            _movieListFavorites = new BindingList<MovieData>(favoriteRepository.GetList());
            _movieListWatchlist = new BindingList<MovieData>(watchlistRepository.GetList());

            //Datasource created in the listBoxes for seamless integration of the main BindingLists created in the program
            //Datasource will updated as the BindList is changed at anytime
            listBoxMovieList.DataSource = _movieList;
            listBoxFavorites.DataSource = _movieListFavorites;
            listBoxWatchlist.DataSource = _movieListWatchlist;

            //value members and display members are created as the form leads to make it mre effienct instead of after the click of the buuttons.
            listBoxMovieList.ValueMember = "id";
            listBoxMovieList.DisplayMember = "title";

            listBoxFavorites.ValueMember = "id";
            listBoxFavorites.DisplayMember = "title";

            listBoxWatchlist.ValueMember = "id";
            listBoxWatchlist.DisplayMember = "title";

        }




        private void buttonSearchMovie_Click(object sender, EventArgs e)
        {
            if (textBoxSearchMovie.Text == null)
            {
                MessageBox.Show("Please enter text before pressing search button.");
                return;
            }
            
            uNoGSService responce = new uNoGSService();
            var errorMessages = string.Empty;
            var list = responce.findMovie(textBoxSearchMovie.Text, out errorMessages);

            if (list == null)
            {
                MessageBox.Show(errorMessages);
                return;
            }


            //in order to bind the initial data to bindinglist we have to place the item in that list instead of calling the method
            foreach (var item in list)
            {
                _movieList.Add(item);

            }
            /*
            listBoxMovieList.DataSource = _movieList;

            listBoxMovieList.ValueMember = "id";
            listBoxMovieList.DisplayMember = "title";
            */


        }


        private void buttonAddToWatchlist_Click(object sender, EventArgs e)
        {
            //items in main list are stored in var
            var item = listBoxMovieList;

            //instance of MovieData is created for us to store the selected item in it
            MovieData equals = (MovieData)item.SelectedItem;
            
            if (!_movieListWatchlist.Any(m => m.id == equals.id))
            {

                //that instance is added to this new listbox for favorite movies
                //call API 
                uNoGSService responce = new uNoGSService();
                
                var fullMovieInfo = responce.getMovie(equals.id);
                watchlistRepository.AddMovie(fullMovieInfo);
                _movieListWatchlist.Add(fullMovieInfo);

                //ValueMen=mber and Displaymember are defined so that it is easy for us to fetch and edit the MovieData instance
                //listBoxWatchlist.ValueMember = "id";
                //listBoxWatchlist.DisplayMember = "title";
            }
        }

        private void listBoxFinalFavorites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //opening new form
            EditMovieData form = new EditMovieData();
            form.Show();
        }

        private void buttonAddToFavorites_Click(object sender, EventArgs e)
        {

            //instance of MovieData is created for us to store the selected item in it
            MovieData equals = (MovieData)listBoxMovieList.SelectedItem;

            //using LinQ statement instead of for loop 
            if (!_movieListFavorites.Any(m => m.id == equals.id))
            {
                uNoGSService responce = new uNoGSService();

                var fullMovieInfo = responce.getMovie(equals.id);
                favoriteRepository.AddMovie(fullMovieInfo);
                //that instance is added to this new listbox for favorite movies
                _movieListFavorites.Add(fullMovieInfo);

                //ValueMen=mber and Displaymember are defined so that it is easy for us to fetch and edit the MovieData instance
                //listBoxFavorites.ValueMember = "id";
                //listBoxFavorites.DisplayMember = "title";
            }

        }

        private void listBoxFavorites_DoubleClick(object sender, EventArgs e)
        {
            //variable item is created
            var item = (ListBox)sender;

            //intance of Moviedata is created to store the selectedd item's value
            MovieData equals = (MovieData)item.SelectedItem;

            //Instance of EditMovieForm is created nad stored in newForm
            var newForm = new EditMovieData();
            //values of equals is set to the values of the the new form just created.
            newForm.MovieToEdit = equals;

            //listener for MovieDataEdited event
            newForm.MovieDataEdited += FavMovieDataEditedHandler;

            newForm.Show();

        }

        private void FavMovieDataEditedHandler(object sender, MovieDataEventArgs e)
        {
            //MessageBox.Show(((EditMovieData)sender).MovieToEdit.movieNotes);
            favoriteRepository.UpdateMovie(e.M);
            MessageBox.Show(e.M.movieNotes);
        }

        private void WatchlistMovieDataEditedHandler(object sender, MovieDataEventArgs e)
        {
            //MessageBox.Show(((EditMovieData)sender).MovieToEdit.movieNotes);
            watchlistRepository.UpdateMovie(e.M);
            MessageBox.Show(e.M.movieNotes);
        }

        private void listBoxWatchlist_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //variable item is created
            var item = (ListBox)sender;

            //intance of Moviedata is created to store the selectedd item's value
            MovieData equals = (MovieData)item.SelectedItem;

            //Instance of EditMovieForm is created nad stored in newForm
            var newForm = new EditMovieData();
            //values of equals is set to the values of the the new form just created.
            newForm.MovieToEdit = equals;

            //listener for MovieDataEdited event
            newForm.MovieDataEdited += WatchlistMovieDataEditedHandler;
            newForm.Show();

        }

        private void buttonMoveToFavorites_Click(object sender, EventArgs e)
        {

            //movingThis vaiable of type MovieData object is created that holds the item selected by the user
            var movingThis = (MovieData)listBoxWatchlist.SelectedItem;

            //if statement to check if anything is slected or not
            //if anything is slected the value of the enumarable will be set to the id of emelent selected
            if (!_movieListFavorites.Any(m => m.id == movingThis.id))
            {
                //added the variable to the other list the object was being moved to 
                favoriteRepository.AddMovie(movingThis);
                _movieListFavorites.Add(movingThis);

            }

            //removing that variable from the list we had the element in and the Datasource ListBox will recognise the change in the Bindlinglist and be updated.
            watchlistRepository.DeleteMovie(movingThis);
            _movieListWatchlist.Remove(movingThis);
           
        }
     
        private void buttonMoveToWatchlist_Click(object sender, EventArgs e)
        {
            //movingThis vaiable of type MovieData object is created that holds the item selected by the user
            var movingThis = (MovieData)listBoxFavorites.SelectedItem;

            //if statement to check if anything is slected or not
            //if anything is slected the value of the enumarable will be set to the id of emelent selected
            if (!_movieListWatchlist.Any(m => m.id == movingThis.id)) {

                //added the variable to the other list the object was being moved to 
                watchlistRepository.AddMovie(movingThis);
                _movieListWatchlist.Add(movingThis);
            
            }

            //removing that variable from the list we had the element in and the Datasource ListBox will recognise the change in the Bindlinglist and be updated.
            favoriteRepository.DeleteMovie(movingThis);
            _movieListFavorites.Remove(movingThis);


        }

        private void radioButtonOrange_CheckedChanged(object sender, EventArgs e)
        {
            //color of the burrons have bneen changed to Blue from the click of the redio button
            
            listBoxFavorites.ForeColor = Color.Red;
            listBoxWatchlist.ForeColor = Color.Red;
            listBoxMovieList.ForeColor = Color.Red;
        }

        private void radioButtonRed_CheckedChanged(object sender, EventArgs e)
        {
            //color of the burrons have bneen changed to Blue
            
            listBoxFavorites.ForeColor = Color.Blue;
            listBoxWatchlist.ForeColor = Color.Blue;
            listBoxMovieList.ForeColor = Color.Blue;
        }

        private void buttonDeleteFromWatchlist_Click(object sender, EventArgs e)
        {

            //variable deletingThis is created that will hold the value of the selected item. taht variable is now an object of the MovieData because of casting
            var deletingThis = (MovieData)listBoxWatchlist.SelectedItem;

            //deletingThis has been removed from the BindingList and hence read by the Datasource listbox and therefore does not show in the listbox as well.
            watchlistRepository.DeleteMovie(deletingThis);
            _movieListWatchlist.Remove(deletingThis);
        }

        private void buttonDeleteFromFavorites_Click(object sender, EventArgs e)
        {
            //variable deletingThis is created that will hold the value of the selected item. taht variable is now an object of the MovieData because of casting
            var deletingThis = (MovieData)listBoxFavorites.SelectedItem;

            //deletingThis has been removed from the BindingList and hence read by the Datasource listbox and therefore does not show in the listbox as well.
            favoriteRepository.DeleteMovie(deletingThis);
            _movieListFavorites.Remove(deletingThis);
        }

        private void listBoxWatchlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxWatchlist.SelectedIndex >= 0)
            {
                //only when somehting in the listbox is selected the buttons for that list will be activated
                buttonDeleteFromWatchlist.Enabled = true;
                buttonMoveToFavorites.Enabled = true;
            }
            else
            {
                //only when somehting in the listbox is selected the buttons for that list will be activated
                buttonDeleteFromWatchlist.Enabled = false;
                buttonMoveToFavorites.Enabled = false;
            }
        }

        private void listBoxFavorites_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if there is any element selected int he listBox then the condition will move ahead with the command
            if (listBoxFavorites.SelectedIndex >= 0)
            {
                //only when somehting in the listbox is selected the buttons for that list will be activated
                buttonDeleteFromFavorites.Enabled = true;
                buttonMoveToWatchlist.Enabled = true;
            }
            else
            {

                //only when somehting in the listbox is not selected the buttons for that list will be deactivated
                buttonDeleteFromFavorites.Enabled = false;
                buttonMoveToWatchlist.Enabled = false;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //starting a save file dialog box
            SaveFileDialog dlg = new SaveFileDialog();

            //add a default extention of xml
            dlg.DefaultExt = "xml";

            //if the user clicks the ok button, we'll do the save
            if (DialogResult.OK == dlg.ShowDialog())
            {

                //using XML writer to write  into the file extention .xlm
                using (XmlWriter writer = XmlWriter.Create(dlg.FileName))
                {
                    //new object of the class FindFlix is created for us to get the values from the list into our xml file
                    FindFlix SezerializedList = new FindFlix();

                    //values from our main binding list are set to the values of new bindling list we had created in FindFLix class which is specifically for us to deal with sending the data outside of the program.
                    SezerializedList.Favorites = _movieListFavorites;
                    SezerializedList.Watchlist = _movieListWatchlist;

                    //serilizing object of FindFlix (SerializedList) and writing the xml doc to a flile using the writer
                    serializer.Serialize(writer, SezerializedList);


                }
                ////instantiate an xmlwritersettings object
                //XmlWriterSettings settings = new XmlWriterSettings();
                //settings.ConformanceLevel = ConformanceLevel.Document;

                ////we'll set indent to true
                //settings.Indent = true;

                ////instantiate xml writer
                //using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                //{
                //    XmlDocument doc = new XmlDocument();
                //    doc.LoadXml("<findFlix></findFlix>");
                //    //first element
                //    var fav = doc.CreateElement("Favorites");

                //    XmlElement newElem;

                //    foreach(var movieData in _movieListFavorites)
                //    {
                //        var movie = doc.CreateElement("movie");
                //        //next element
                //        newElem = doc.CreateElement("id");
                //        newElem.InnerText = movieData.id.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Title");
                //        newElem.InnerText = movieData.title.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Genre");
                //        newElem.InnerText = movieData.genre.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("ReleaseDate");
                //        newElem.InnerText = movieData.releaseDate.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Rated");
                //        newElem.InnerText = movieData.rated.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("ImdbRating");
                //        newElem.InnerText = movieData.imdbRating.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Director");
                //        newElem.InnerText = movieData.director.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Writer");
                //        newElem.InnerText = movieData.writer.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("PLot");
                //        newElem.InnerText = movieData.plot.ToString();
                //        movie.AppendChild(newElem);

                //        newElem = doc.CreateElement("Notes");
                //        newElem.InnerText = movieData.movieNotes.ToString();
                //        movie.AppendChild(newElem);

                //        fav.AppendChild(movie);
                //    }

                //    var fav1 = doc.CreateElement("Watchlist");

                //    XmlElement newElem1;

                //    foreach (var movieData in _movieListWatchlist)
                //    {
                //        var movie = doc.CreateElement("movie");
                //        //next element
                //        newElem1 = doc.CreateElement("id");
                //        newElem1.InnerText = movieData.id.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Title");
                //        newElem1.InnerText = movieData.title.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Genre");
                //        newElem1.InnerText = movieData.genre.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("ReleaseDate");
                //        newElem1.InnerText = movieData.releaseDate.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Rated");
                //        newElem1.InnerText = movieData.rated.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("ImdbRating");
                //        newElem1.InnerText = movieData.imdbRating.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Director");
                //        newElem1.InnerText = movieData.director.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Writer");
                //        newElem1.InnerText = movieData.writer.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("PLot");
                //        newElem1.InnerText = movieData.plot.ToString();
                //        movie.AppendChild(newElem1);

                //        newElem1 = doc.CreateElement("Notes");
                //        newElem1.InnerText = movieData.movieNotes.ToString();
                //        movie.AppendChild(newElem1);

                //        fav1.AppendChild(movie);
                //    }
                //    doc.DocumentElement.AppendChild(fav);
                //    doc.DocumentElement.AppendChild(fav1);

                //    doc.Save(writer);

                    //document saved as XML message
                    MessageBox.Show("File saved as an XML.");
                
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //using FileStream instead of StreamReader for change to use something different to read files
            //using the full address for now (C:\Users\NRangi\Source\Repos\project-and-portfolio-3\MovieData.xml) will make this more efficient later
            using (FileStream stream = File.OpenRead(@"C:\Users\NRangi\Source\Repos\project-and-portfolio-3\MovieData.xml"))
            {
                //Creating an object of FindFlix class to set the values of of BindingList in the that class and from there to our main class.
                FindFlix dezerializedList = (FindFlix)serializer.Deserialize(stream);

                //Setting the values of our main Binding list from our FindFlix class BindingList which currently holds the values.
                _movieListFavorites = dezerializedList.Favorites;
                _movieListWatchlist = dezerializedList.Watchlist;

                //Our ListBoxes' datasource has not been bound by the Bindling list we just added values to.
                listBoxFavorites.DataSource = _movieListFavorites;
                listBoxWatchlist.DataSource = _movieListWatchlist;

            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //using openlifeDialog to set the location of whweer the text file is going to be
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Documents|*.txt", ValidateNames = true}) 
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter sr = new StreamWriter(ofd.FileName))
                    {
                        
                        sr.WriteLine("_______________YOUR MOVIE LISTS______________");
                        sr.WriteLine("");
                        sr.WriteLine("=============================================");
                        sr.WriteLine("_ _ _ _ _ _ _ _FAVORITE MOVIES _ _ _ _ _ _ _");
                       
                        foreach (var  movieData in _movieListFavorites)
                        {
                            sr.WriteLine("");
                            sr.WriteLine("_______________");
                            sr.WriteLine("    Title                 {0}", movieData.title);
                            sr.WriteLine("    Genre                 {0}", movieData.genre);
                            sr.WriteLine("    Release Date          {0}", movieData.releaseDate);
                            sr.WriteLine("    Rated                 {0}", movieData.rated);
                            sr.WriteLine("    IMDB Rating           {0}", movieData.imdbRating);
                            sr.WriteLine("    Director              {0}", movieData.director);
                            sr.WriteLine("    Writer                {0}", movieData.writer);
                            sr.WriteLine("");
                            sr.WriteLine("    Your Notes");
                            sr.WriteLine("    {0}", movieData.movieNotes);


                        }
                        sr.WriteLine("");
                        sr.WriteLine("=============================================");
                        sr.WriteLine("_ _ _ _ _ _ _MOVIES TO WATCH LATER_ _ _ _ _ _");

                        foreach (var movieData2 in _movieListWatchlist)
                        {
                            sr.WriteLine("");
                            sr.WriteLine("_______________");
                            sr.WriteLine("    Title                 {0}", movieData2.title);
                            sr.WriteLine("    Genre                 {0}", movieData2.genre);
                            sr.WriteLine("    Release Date          {0}", movieData2.releaseDate);
                            sr.WriteLine("    Rated                 {0}", movieData2.rated);
                            sr.WriteLine("    IMDB Rating           {0}", movieData2.imdbRating);
                            sr.WriteLine("    Director              {0}", movieData2.director);
                            sr.WriteLine("    Writer                {0}", movieData2.writer);
                            sr.WriteLine("");
                            sr.WriteLine("    Your Notes");
                            sr.WriteLine("    {0}", movieData2.movieNotes);


                            sr.WriteLine("_______________");
                            sr.WriteLine("");
                        }
                        sr.WriteLine("=============================================");

                        sr.Close();
                    }

                }
            
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exiting the application.
            Application.Exit();
        }

    }
}
