﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;

namespace RangiNishant_FindFlix
{
    class FavoriteRepository
    {
        MySqlConnection _conn;
        string serverIP = string.Empty;

        private string ConnectionString
        {
            get
            {

                if (string.IsNullOrEmpty(serverIP))
                {
                    //openn text file to open stream reader
                    using (StreamReader sr = new StreamReader("connect.txt"))
                    {
                        //read the server IP data
                        serverIP = sr.ReadToEnd();
                    }
                }
                //retur the entire string 
                return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=Movie_Database_data;port=8889";
            }
        }

        private MySqlConnection MyConnection
        {
            get
            {
                if (_conn == null)
                {
                    _conn = new MySqlConnection();
                    _conn.ConnectionString = ConnectionString;
                }
                return _conn;
            }
        }

        public List<MovieData> GetList()
        {
            string stm = "SELECT * FROM favoriteListDB LIMIT 25;";
            MySqlCommand myCommand = new MySqlCommand(stm, MyConnection);
            myCommand.Connection.Open();
            MySqlDataReader rdr = myCommand.ExecuteReader();
            List<MovieData> movieList = new List<MovieData>();
            
            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    MovieData v = new MovieData();
                    //Fill Values from DB
                    v.id = rdr["movieId"].ToString();
                    v.title = rdr["title"].ToString();
                    v.genre = rdr["genre"].ToString();
                    v.releaseDate = int.Parse(rdr["releaseDate"].ToString());
                    v.rated = rdr["rated"].ToString();
                    v.imdbRating = decimal.Parse(rdr["imdbRating"].ToString());
                    v.director = rdr["director"].ToString();
                    v.writer = rdr["writer"].ToString();
                    v.plot = rdr["plot"].ToString();
                    v.movieNotes = rdr["movieNotes"].ToString();

                    movieList.Add(v);
                }
                rdr.Close();
            }
            _conn.Close();
            return movieList;
        }

        public void AddMovie(MovieData movie)
        {
            string myExecuteQuery = "INSERT INTO favoriteListDB VALUES ( @movieId, @title,@genre, @releaseDate,@rated ,@imdbRating,@director,@writer,@plot,@movieNotes)";

            MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, _conn);
            myCommand.Connection.Open();

            try
            {

                myCommand.Parameters.AddWithValue("@movieId", movie.id);
                myCommand.Parameters.AddWithValue("@title", movie.title);
                myCommand.Parameters.AddWithValue("@genre", movie.genre);
                myCommand.Parameters.AddWithValue("@releaseDate", movie.releaseDate);
                myCommand.Parameters.AddWithValue("@rated", movie.rated);
                myCommand.Parameters.AddWithValue("@imdbRating", movie.imdbRating);
                myCommand.Parameters.AddWithValue("@director", movie.director);
                myCommand.Parameters.AddWithValue("@writer", movie.writer);
                myCommand.Parameters.AddWithValue("@plot", movie.plot);
                myCommand.Parameters.AddWithValue("@movieNotes", movie.movieNotes);

                myCommand.ExecuteNonQuery();
            }
            finally
            {
                if (_conn.State == System.Data.ConnectionState.Open) 
                {
                    _conn.Close();
                }
                
            }
            

        }

        public void UpdateMovie(MovieData movie)
        {
            string myExecuteQuery = "UPDATE favoriteListDB SET title = @title, year = @releaseDate, genre = @genre, releaseDate = @releaseDate, rated = @rated, imdbRating = @imdbRating, director = @director, writer = @writer, plot = @plot, movieNotes = @movieNotes  WHERE id = @movieId";

            MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, _conn);
            myCommand.Connection.Open();

            myCommand.Parameters.AddWithValue("@movieId", movie.id);
            myCommand.Parameters.AddWithValue("@title", movie.title);
            myCommand.Parameters.AddWithValue("@genre", movie.genre);
            myCommand.Parameters.AddWithValue("@releaseDate", movie.releaseDate);
            myCommand.Parameters.AddWithValue("@rated", movie.rated);
            myCommand.Parameters.AddWithValue("@imdbRating", movie.imdbRating);
            myCommand.Parameters.AddWithValue("@director", movie.director);
            myCommand.Parameters.AddWithValue("@writer", movie.writer);
            myCommand.Parameters.AddWithValue("@plot", movie.plot);
            myCommand.Parameters.AddWithValue("@movieNotes", movie.movieNotes);

            myCommand.ExecuteNonQuery();
            _conn.Close();
        }

        public void DeleteMovie(MovieData movie)
        {
            string myExecuteQuery = "DELETE FROM favoriteListDB WHERE movieId = @id";

            MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, _conn);
            myCommand.Connection.Open();
            myCommand.Parameters.AddWithValue("@id", movie.id);
            myCommand.ExecuteNonQuery();
            _conn.Close();
        }
    }
}
