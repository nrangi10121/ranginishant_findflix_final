_______________YOUR MOVIE LISTS______________

=============================================
_ _ _ _ _ _ _ _FAVORITE MOVIES _ _ _ _ _ _ _

_______________
    Title                 Home Alone 2: Lost in New York
    Genre                 Adventure, Comedy, Crime, Family
    Release Date          1992
    Rated                 PG
    IMDB Rating           6.8
    Director              Chris Columbus
    Writer                John Hughes (characters), John Hughes

    Your Notes
    This was not such a great movie after watching the first one.

_______________
    Title                 Home
    Genre                 Animation, Adventure, Comedy, Family, Fantasy, Sci-Fi
    Release Date          2015
    Rated                 PG
    IMDB Rating           6.6
    Director              Tim Johnson
    Writer                Tom J. Astle (screenplay by), Matt Ember (screenplay by), Adam Rex (book)

    Your Notes
    

=============================================
_ _ _ _ _ _ _MOVIES TO WATCH LATER_ _ _ _ _ _

_______________
    Title                 Home Alone
    Genre                 Comedy, Family
    Release Date          1990
    Rated                 PG
    IMDB Rating           7.6
    Director              Chris Columbus
    Writer                John Hughes

    Your Notes
    Trump was in this movie, what a surprise.
_______________


_______________
    Title                 Jeff, Who Lives at Home
    Genre                 Comedy, Drama
    Release Date          2011
    Rated                 R
    IMDB Rating           6.5
    Director              Jay Duplass, Mark Duplass
    Writer                Jay Duplass, Mark Duplass

    Your Notes
    
_______________

=============================================
